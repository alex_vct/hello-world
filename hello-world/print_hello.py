from typing import NoReturn


def print_hello(hello_str: str) -> None:
    print(hello_str)


if __name__ == "__main__":
    print_hello("hello world!")
